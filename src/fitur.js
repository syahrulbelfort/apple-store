import { MdOutlineLocalShipping } from 'react-icons/md'


export const FITURS = [
  {
    icon: <MdOutlineLocalShipping/>,
    title: "GRATIS ONGKIR",
    text: "Pengiriman Terpercaya oleh EKADELIVERY. Gratis se-Jabodetabek dan Surabaya!"
  },
  
  {
    icon: <MdOutlineLocalShipping/>,
    title: "CICILAN 12 BULAN",
    text: "Kamu bisa bertransaksi dengan Cicilan Kartu Kredit maupun Kredivo/Akulaku."
  },
  
  {
    icon: <MdOutlineLocalShipping/>,
    title: "ORIGINAL 100%",
    text: "Garansi Uang Kembali jika tidak ORIGINAL."
  },
  
  {
    icon: <MdOutlineLocalShipping/>,
    title: "GARANSI 1 TAHUN",
    text: "Garansi Apple hingga 1 tahun untuk pembelian unit NEW dan Garansi 2 minggu Ganti Unit untuk Produk Second."
  },
];