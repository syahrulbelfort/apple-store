import React from 'react';
import { Slide } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css'
import './slider.css'

const divStyle = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  backgroundSize: 'cover',
  height: '350px',
    width: '1000px',
    margin:'auto',
    marginTop:'10px',
    backgroundColor:'#F8F8F8'
}
const slideImages = [
  {
    url: 'images/iphone13.png',
  },
  {
    url: 'images/iphone14.png',
  },
  {
    url: 'images/macbookpro.png',
  },
];

const Slideshow = () => {
    return (
      <div className="slide-container">
        <Slide>
         {slideImages.map((slideImage, index)=> (
            <div key={index}>
              <div className='divStyle' style={{ ...divStyle, 'backgroundImage': `url(${slideImage.url})` }}>
              </div>
            </div>
          ))} 
        </Slide>
      </div>
    )
}

export default Slideshow