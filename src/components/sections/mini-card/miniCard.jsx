import React from 'react'
import './miniCard.css'
export default function miniCard() {
  return (
    <>
    <div className="container-miniCard">
    <div className='layout-card'>
        <div className="card-text">            
       <h1>Airpods Pro</h1> 
        <a href=''>BELANJA SEKARANG</a>
        </div>
        <div className="card-img">            
        <img style={{width:'150px'}} src='/images/airpods.png'/>
        </div>
    </div>
    <div className='layout-card'>
        <div className="card-text">            
       <h1>Ipad Pro</h1> 
        <a href=''>BELANJA SEKARANG</a>
        </div>
        <div className="card-img">            
        <img style={{width:'180px'}} src='/images/2ipad.png'/>
        </div>
    </div>
    </div>
    </>

  )
}
