import React from 'react'
import './banner3.css'
import '../banner-1/banner1.css'
export default function banner3() {
  return (
    <> 
    <div className="container-banner-3">
    <div className='container-banner3'>
        <div className="banner3-img">
            <img style={{width:'300px'}} src='images/3-1.png'/>
        </div>
        <div className="banner3-text">
            <h1>IPHONE 12 & IPHONE 12 MINI</h1>
            <p>Mulai Dari 10 JUTA-AN</p>
            <p className='banner3-desc'>A14 Bionic, chip paling cepat di ponsel pintar.
                Layar OLED menyeluruh. Ceramic Shield dengan ketahanan jatuh empat kali lebih baik. Dan mode Malam di setiap kameranya. iPhone 12 punya semuanya — dalam dua ukuran sempurna.</p>
                <button className='banner1-button' style={{marginTop:'30px'}}>belanja sekarang</button>
        </div>

    </div>
    </div>

    </>
  )
}
