import React from 'react'
import '../banner-2/banner2.css'

export default function Fitur(props) {
    const {icon, title, text} = props
    
  return (
    <>
        <div className=" icon-shape">
        <h1> {icon} </h1>
        </div>
        <div className="container-banner2-text">
          <h1>{title}</h1>
        <p>{text}</p>
        </div>
</>
     

  )
}
