import { FITURS } from '../../../fitur.js'
import Fitur from './cards'
import React from 'react'
import Carousel from '../../sliders/carousel.jsx'

export default function section2() {
  
  return (

    <div className="container-banner2">
      <Carousel/>
    <div className='container-banner2-icon'>{FITURS.map((fitur)=>(
        <Fitur 
         icon={fitur.icon}
         title={fitur.title}
         text={fitur.text}
        />
    ))}</div>
    </div>

  )
}
