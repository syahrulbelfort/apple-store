import React from 'react'
import './banner1.css'
function banner1() {
  return (
    <div className='container'>
        <div className="container-banner1">
            <div className="description-text-banner1">
                <h1>KREDIT IPHONE 14, DISKON 2 JUTA!</h1>
                <p>Berlaku untuk pembelian iPhone 14, iPhone 14 Plus, iPhone 14 Pro dan iPhone 14 Pro Max di Seluruh Outlet EKACELULLER dan Website EKACELULLER.COM dengan metode pembayaran Kredivo.</p>
                <button className='banner1-button'>Selengkapnya</button>
            </div>
            <div className="images">
                <img src='/images/ip14pro.png' />
            </div>
        </div>
    </div>
  )
}

export default banner1