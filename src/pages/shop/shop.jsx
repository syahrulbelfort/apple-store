import React from 'react'
import {PRODUCTS} from '../../products'
import { Product } from './product'
import './shop.css'
import Banner1 from '../../components/sections/banner-1/banner1'
import Banner2 from '../../components/sections/banner-2/banner2'
import Banner3 from '../../components/sections/banner-3/banner3'
import Minicard from '../../components/sections/mini-card/miniCard'
import Footer from '../../components/footer/footer'
import { FloatingWhatsApp } from 'react-floating-whatsapp'

export const Shop = () => {
  return (
    
    <div className='shop'>
      <Banner1/>
      <Banner2/>
      <Minicard/>
      <Banner3/>
     <div className="shopTitle">
        <h1>BEST SELLING PRODUCTS</h1>
     </div>
     
     <div className="products">
       {PRODUCTS.map((product) => (
        <Product data={product}/>
       ))}
     </div>
     <FloatingWhatsApp 
     phoneNumber="6285714053878"
     accountName="Syahrul Kurniawan"
     chatMessage="Hello there! 🤝 How can i help?"
     avatar="/images/me.png"
     />

     <Footer/>
    </div>
  )
}
