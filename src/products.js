import product1 from "./assets/products/1.png";
import product2 from "./assets/products/2.png";
import product3 from "./assets/products/3.png";
import product4 from "./assets/products/4.png";
import product5 from "./assets/products/5.png";
import product6 from "./assets/products/6.png";


export const PRODUCTS = [
  {
    id: 1,
    productName: "IPhone 12",
    price: 999.0,
    productImage: product1,
  },
  {
    id: 2,
    productName: "Macbook Pro 2022 (M1)",
    price: 1999.0,
    productImage: product2,
  },
  {
    id: 3,
    productName: "iPhone 11",
    price: 699.0,
    productImage: product3,
  },
  {
    id: 4,
    productName: "iphone 12",
    price: 228.0,
    productImage: product4,
  },
  {
    id: 5,
    productName: "iPhone 13",
    price: 19.99,
    productImage: product5,
  },
  {
    id: 6,
    productName: "iPhone 11 Purple",
    price: 68.0,
    productImage: product6,
  }
];